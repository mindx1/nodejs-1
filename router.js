const express = require('express');
const mongoose = require('mongoose')

const Schema = mongoose.Schema;

const kittySchema = new Schema({
    title: String,
    description: String,
})

const Kitten = mongoose.model('kitten', kittySchema)

// const tho = new Kitten({ title: 'post 1', description: 'post 1 des', tags: [{ two: 2 }] })
// tho.save();

const router = express.Router();

router.get('/', (req, res) => {
    res.send({ a: 1, ba: 2 })
})

router.get('/student', (req, res) => {
    res.send({ a: 1, b: 2 })
})


router.post('/student', async (req, res) => {
    console.log(req.body)
    const { description, title } = req.body;
    const post = new Kitten({ description, title })
    const result = await post.save();
    res.send(result)
})

module.exports = router;