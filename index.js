// const http = require('http')

// const server = http.createServer(function (req, res) {
//     if (req.url === '/') {
//         res.writeHead(200, { "Content-Type": "text/html" })
//         res.write('<h1>Trang chu</h1>')
//         res.end();
//     } else if (req.url === '/student') {
//         res.writeHead(200, { "Content-Type": "text/html" })
//         res.write('<h1>Student</h1>')
//         res.end();
//     }

// })

// server.listen(4000)

const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser')

const Schema = mongoose.Schema;

const router = require('./router')

const app = express();

app.use(bodyParser.json())

mongoose.connect('mongodb://127.0.0.1:27017/tho', { useNewUrlParser: true, useUnifiedTopology: true }, () => {
    console.log('database connect')
})

const kittySchema = new Schema({
    title: String,
    description: String,
})

// const Kitten = mongoose.model('kitten', kittySchema)

// const tho = new Kitten({ title: 'post 1', description: 'post 1 des', tags: [{ two: 2 }] })
// tho.save();

app.use('/', (req, res, next) => {
    if ('ok') {
        next();
    } else {
        res.send('Khong duoc quyen')
    }
})

app.use('/', router)



app.listen(4000, () => {
    console.log('server run on port 4000')
})